package db

import (
	"testing"
	"time"
)

const (
	collectiveName = "collective"
	collectiveHost = "host"
)

func TestAddCountCollective(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	count, err := db.CountCollectives(collectiveHost)
	if err != nil {
		t.Fatalf("Got an error counting collectives: %v", err)
	}
	if count != 0 {
		t.Errorf("Got an unexpected number of collectives: %d", count)
	}

	err = db.AddCollective(collectiveName, collectiveHost)
	if err != nil {
		t.Fatalf("Got an error adding a collective: %v", err)
	}

	count, err = db.CountCollectives(collectiveHost)
	if err != nil {
		t.Fatalf("Got an error counting collectives: %v", err)
	}
	if count != 1 {
		t.Errorf("Got an unexpected number of collectives: %d", count)
	}

	err = db.AddCollective(collectiveName+"1", collectiveHost)
	if err != nil {
		t.Fatalf("Got an error adding a collective: %v", err)
	}
	err = db.AddCollective(collectiveName+"2", collectiveHost)
	if err != nil {
		t.Fatalf("Got an error adding a collective: %v", err)
	}

	count, err = db.CountCollectives(collectiveHost)
	if err != nil {
		t.Fatalf("Got an error counting collectives: %v", err)
	}
	if count != 3 {
		t.Errorf("Got an unexpected number of collectives: %d", count)
	}
}

func TestExpireCollectives(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	err := db.AddCollective(collectiveName, collectiveHost)
	if err != nil {
		t.Fatalf("Got an error adding a collective: %v", err)
	}

	count, err := db.CountCollectives(collectiveHost)
	if err != nil {
		t.Fatalf("Got an error counting collectives: %v", err)
	}
	if count != 1 {
		t.Errorf("Got an unexpected number of collectives: %d", count)
	}

	err = db.ExpireCollectives(time.Microsecond)
	if err != nil {
		t.Fatalf("Got an error expiring invites: %v", err)
	}

	count, err = db.CountCollectives(collectiveHost)
	if err != nil {
		t.Fatalf("Got an error counting collectives: %v", err)
	}
	if count != 0 {
		t.Errorf("Got an unexpected number of collectives: %d", count)
	}
}
