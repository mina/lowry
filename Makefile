all: js go

js:
	npm install
	npm install --save-dev expose-loader
	npm run build
	mkdir -p dist/js
	cp assets/zxcvbn.js dist/js
	cp assets/zxcvbn-bootstrap-strength-meter.js dist/js
	cp assets/jquery.dynatable* dist/js

go:
	go get .
	go build

clean:
	rm -rf node_modules dist lowry

deps:
	sudo apt install ldap-utils golang npm

run_ldap:
	docker run --rm -d -p 389:3389 --name ldap registry.sindominio.net/ldap
	sleep 1
	ldapadd -x -w password -H ldap://localhost:389/ -f examples/data.ldif -D "cn=admin,dc=nodomain"

demo:
	./lowry -noLockUsers -config examples/lowry.conf

test:
	go test ./...
