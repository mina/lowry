package ldap

import (
	"log"
	"strings"
)

type Locked int

const (
	Unlocked Locked = iota
	Blocked
	Deleted
	Unknown
)

func LockedFromString(s string) Locked {
	switch strings.ToLower(s) {
	case "":
		return Unlocked
	case "unlocked":
		return Unlocked
	case "blocked":
		return Blocked
	case "deleted":
		return Deleted
	default:
		log.Printf("Not valid locked status: %s", s)
		return Unknown
	}
}

func (r Locked) String() string {
	switch r {
	case Unlocked:
		return ""
	case Blocked:
		return "blocked"
	case Deleted:
		return "deleted"
	default:
		return "unknown"
	}
}
